import matplotlib.pyplot as plt
from math import sin, cos, pi

#################
#   Aufgabe 1   #
#################

def aufgabe1_a(args):
    r_min = args["r_min"]
    r_max = args["r_max"]
    r_N = args["r_N"]
    N = args["N"]
    precision = args["precision"]

    for r_i in range(r_N+1):
        r = r_min + (r_max - r_min) * r_i / r_N
        x = args["x_start"]
        for i in range(N):
            x = round(r * x * (1 - x), precision)
        x_n = []
        while x not in x_n:
            x_n.append(x)
            x = round(r * x * (1 - x), precision)
        plt.plot([r] * len(x_n), x_n, '.', color = 'black', markersize = 0.5)

    plt.savefig(args["filename"])
    plt.close()

def aufgabe1_b(x_start, precision):
    R = [1.5, 2.7, 3.2, 3.46, 3.56, 3.57, 3.58, 3.59]
    for r in R:
        x = x_start
        x_n = []
        n = 0
        while x not in x_n:
            x_n.append(x)
            x = round(r * x * (1 - x), precision)
            n += 1

        plt.subplot(2, 1, 1)
        plt.plot(range(n), x_n)
        plt.title("r = "+str(r))
        plt.xlabel("n = "+str(n))

        plt.subplot(2, 1, 2)
        plt.plot(x_n[:-1], x_n[1:])
        plt.xlabel("precision = "+str(precision))

        plt.tight_layout()
        plt.savefig("aufgabe1_b_r="+str(r).replace(".",",")+"_x0="+str(x_start).replace(".",","))
        plt.close()

def aufgabe1_c(x_start, precision):
    N = 1000
    
    # part 1
    plt.subplot(2, 1, 1)
    R = [3.67, 3.68, 3.69, 3.7, 3.71]
    for r in R:
        x = x_start
        for i in range(N):
            x = round(r * x * (1 - x), precision)
        x_n = []
        while x not in x_n:
            x_n.append(x)
            x = round(r * x * (1 - x), precision)

        plt.plot([r] * len(x_n), x_n, '.')
    plt.ylim(0.68, 0.76)

    # part 2
    plt.subplot(2, 1, 2)
    R = [3.7, 3.701, 3.702, 3.703, 3.704]
    for r in R:
        x = x_start
        for i in range(N):
            x = round(r * x * (1 - x), precision)
        x_n = []
        while x not in x_n:
            x_n.append(x)
            x = round(r * x * (1 - x), precision)
        plt.plot([r] * len(x_n), x_n, '.')
    plt.ylim(0.7, 0.708)

    plt.tight_layout()
    plt.savefig("aufgabe1_c")
    plt.close()



#################
#   Aufgabe 2   #
#################

def simulate_pendulum(omega_N, D, A, d_t, phi_0, psi_0, precision):
    phi = phi_0
    psi = psi_0
    omega = omega_N
    phases = [(phi, omega)]

    while True:
        phi_1 = phi + d_t * omega
        omega_1 = omega + d_t * (-D * omega + sin(phi) + A * cos(psi))
        psi_1 = psi + d_t * omega_N
        phi = round(phi_1, precision)
        omega = round(omega_1, precision)
        psi = round(psi_1, precision)
        phases.append((phi, omega))
        print(phi, omega)
        if (phi, omega) == (phi_0, omega_N): break

    return phases

phases = simulate_pendulum(0.8, 0.7, 1.8, 0.001, pi/2, 0, 3)
phi, omega = zip(*phases)
plt.plot(phi, omega)
plt.show()
print(len(phases))
    

















