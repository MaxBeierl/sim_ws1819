# -*- coding: utf-8 -*-
"""
Created on Sun Nov 18 20:23:28 2018

@author: Besitzer
"""

import numpy as np
from random import random
from math import sqrt

# x_0: state of system at start
# t_start: starting temperatur
# get_neighbour: function returning neighbour state;
#   params: x - current state
# get_c: function retruning value which is to minimize;
#   params: y - state to evaluate
# get_p_accept: fuction returning probability of accepting next state;
#   params: c_k - current value of state
#       c_k+1 - value of neighbour state
#       t - temperature (evaluate with get_t)
# get_t: function returning current temperature according to cooling shedule
#   params: t - current temperature
#       k - number of iterations
# is_solution: returns whether breaking criteria fulfilled
#   params: c - history of state values
# returns: history of state values
def simulated_annealing_alg(x_0, t_start, get_neighbour, get_c, get_p_accept, get_t, is_solution):
    # step 1: initialize algorithm
    x = x_0
    t = t_start
    k = 1           # count iterations
    c = [get_c(x)]  # history of state values
    t_history = [t_start]
    
    # algorithm loop - do while breaking criteria not fulfilled
    while True:    
        # step 2: get neighbour and evaluate stat
        y = get_neighbour(x)
        c_y = get_c(y)
        
        # DEBUG
        if k % 100 == 0:
            print(c[-1], c_y, "; p =", get_p_accept(c[-1], c_y, t), "; t =", t)
        
        # step 3: test for replacing state
        if random() < get_p_accept(c[-1], c_y, t):
            x = y
            c.append(c_y)
        else:
            c.append(c[-1])
        t_history.append(t)
        
        # step 4: get new temperature
        t = get_t(t, k)
        
        # step 5: test breaking criteria
        if is_solution(c):
            break
        else:
            k += 1
    return c, t_history


# returns energy value of magnet according to formula
# s: 3-dimensional numpy array representing magnet
# J_0: 1 or -1
def get_H(s, J_0):
    x_max, y_max, z_max = s.shape
    H_sum = 0
    for x in range(x_max):
        for y in range(y_max):
            for z in range(z_max):
                temp_sum = 0
                if x > 0: temp_sum += J_0 * s[x,y,z] * s[x-1, y, z]
                if x < x_max-1: temp_sum += J_0 * s[x,y,z] * s[x+1, y, z]
                if y > 0: temp_sum += J_0 * s[x,y,z] * s[x,y-1, z]
                if y < y_max-1: temp_sum += J_0 * s[x,y,z] * s[x,y+1,z]
                if z > 0: temp_sum += J_0 * s[x,y,z] * s[x,y,z-1]
                if z < z_max-1: temp_sum += J_0 * s[x,y,z] * s[x,y,z+1]
                H_sum += temp_sum
    H_sum *= -1/2
    return H_sum


#################################
#   GET_NEIGHBOUR FUNCTIONS     #
#################################

# returns neighbour magnet
# a random point in 3D space is taken and value flipped
# all points along the 3 axes with same value also get flipped (till first diffrent)
# s: 3-dimensinal numpy array representing magnet
def neighbour_axis_fill(s):
    neighbour = s.copy()
    x_max, y_max, z_max = neighbour.shape
    x = int(random()*x_max)
    y = int(random()*y_max)
    z = int(random()*z_max)
    flip_value = -1 * neighbour[x,y,z]
    
    neighbour[x,y,z] = flip_value
    for x_i in range(x+1, x_max):
        if neighbour[x_i,y,z] != flip_value: neighbour[x_i,y,z] = flip_value
        else: break
    for x_i in range(x-1, -1, -1):
        if neighbour[x_i,y,z] != flip_value: neighbour[x_i,y,z] = flip_value
        else: break
    for y_i in range(y+1, y_max):
        if neighbour[x,y_i,z] != flip_value: neighbour[x,y_i,z] = flip_value
        else: break
    for y_i in range(y-1, -1, -1):
        if neighbour[x,y_i,z] != flip_value: neighbour[x,y_i,z] = flip_value
        else: break
    for z_i in range(z+1, z_max):
        if neighbour[x,y,z_i] != flip_value: neighbour[x,y,z_i] = flip_value
        else: break
    for z_i in range(z-1, -1, -1):
        if neighbour[x,y,z_i] != flip_value: neighbour[x,y,z_i] = flip_value
        else: break
    
    return neighbour

# returns neighbour magnet via floodfill (ITERATIVE IMPLEMENTATION)
# a random point in 3D space is taken and value flipped
# all adjectient points with same value also get flipped
# s: 3-dimensinal numpy array representing magnet
def neighbour_floodfill_iterative(s):
    neighbour = s.copy()
    x_max, y_max, z_max = neighbour.shape
    x = int(random()*x_max)
    y = int(random()*y_max)
    z = int(random()*z_max)
    flip_value = -1 * neighbour[x,y,z]
    
    stack = [(x, y, z)]
    while stack:
        x, y, z = stack.pop()
        neighbour[x,y,z] = flip_value
        if x > 0: 
            if neighbour[x-1,y,z] != flip_value: 
                stack.append((x-1, y, z,))
        if x < x_max-1: 
            if neighbour[x+1,y,z] != flip_value: 
                stack.append((x+1, y, z))
        if y > 0: 
            if neighbour[x,y-1,z] != flip_value: 
                stack.append((x, y-1, z))
        if y < y_max-1: 
            if neighbour[x,y+1,z] != flip_value: 
                stack.append((x, y+1, z))
        if z > 0: 
            if neighbour[x,y,z-1] != flip_value: 
                stack.append((x, y, z-1))
        if z < z_max-1: 
            if neighbour[x,y,z+1] != flip_value: 
                stack.append((x, y, z+1))
    return neighbour
    

# returns neighbour magnet by flipping random values
# a random point in 3D space is taken and value flipped
# s: 3-dimensinal numpy array representing magnet
# n: number of points to flip
#   default = 1
#   NOTE: by chance the same point could be flipped more times
def neighbour_flip_point(s, n = 1):
    neighbour = s.copy()
    x_max, y_max, z_max = neighbour.shape
    for i in range(n):
        x = int(random()*x_max)
        y = int(random()*y_max)
        z = int(random()*z_max)
        neighbour[x,y,z] *= -1
    return neighbour

# returns neighbour magnet by optimizing cube
# a random point in 3D space is taken
# all possible cubes with side length = (size + 1 + size) in every direction get tested
# magnet with best value returned
# s: 3-dimensional numpy array representing array
# get_c: function retruning value which is to minimize;
#   params: y - state to evaluate
# size: expansion in every direction
def neigbour_optimize_cube(s, get_c, size = 1):
    neighbour = s.copy()
    x_max, y_max, z_max = neighbour.shape
    # select random middle point of cube
    x = int(random()*x_max)
    y = int(random()*y_max)
    z = int(random()*z_max)
    # get possible boundaries for the cube
    x_lower = max([x-size, 0])
    x_upper = min([x+size+1, x_max])
    y_lower = max([y-size, 0])
    y_upper = min([y+size+1, y_max])
    z_lower = max([z-size, 0])
    z_upper = min([z+size+1, z_max])
    x_len = x_upper - x_lower
    y_len = y_upper - x_lower
    z_len = z_upper - z_lower
    # get possible boundaries for cube evaluation portion
    x_lower_test = max([x_lower-1, 0])
    x_upper_test = min([x_upper+1, x_max])
    y_lower_test = max([y_lower-1, 0])
    y_upper_test = min([y_upper+1, y_max])
    z_lower_test = max([z_lower-1, 0])
    z_upper_test = min([z_upper+1, z_max])   
    # test all possible cubes
    best_cube = 0
    min_value = float("inf")
    for cube in get_cubes(x_len * y_len * z_len):
        cube = np.reshape(cube, (x_len, y_len, z_len))
        neighbour[x: x+x_len, y: y+y_len, z: z+z_len] = cube
        cur_value = get_c(neighbour[x_lower_test:x_upper_test, y_lower_test:y_upper_test, z_lower_test:z_upper_test])
        if cur_value < min_value:
            min_value = cur_value
            best_cube = cube
    neighbour[x: x+x_len, y: y+y_len, z: z+z_len] = best_cube
    return neighbour
        
def optimize_neighbour(s, get_c):
    neighbour = s.copy()
    x_max, y_max, z_max = neighbour.shape
    # select random middle point of cube
    x = int(random()*x_max)
    y = int(random()*y_max)
    z = int(random()*z_max)
    neighbour[x,y,z] = 1
    c_1 = get_c(neighbour[max(x-1, 0):min(x_max, x+2),max(y-1, 0):min(y_max, x+2),max(z-1, 0):min(z_max, x+2)])
    neighbour[x,y,z] = -1
    c_2 = get_c(neighbour[max(x-1, 0):min(x_max, x+2),max(y-1, 0):min(y_max, x+2),max(z-1, 0):min(z_max, x+2)])
    if c_1 < c_2: neighbour[x,y,z] = 1
    return neighbour
        

#######################
#   GET_P FUNCTIONS   #
#######################

# returns pobabiltiy of accepting the new solution
# using the standard exponential function
# temperature -> 0 => p -> 0
# c_new - c_curr -> +inf => p -> 0
# c_curr: current value of state
# c_new: value of neighbour state
# t: temperature (evaluate with get_t)
def get_p_accept_standard(c_curr, c_new, t):
    if c_new <= c_curr: return 1
    if t <= 0: return 0
    return np.exp((c_curr - c_new)/t)
 

#########################
#   GET_T FUNCTIONS     #
#########################
   
# returns new temperature
# using a linear cooling shedule
#   NOTE: if no delta_t == 0 specified its exponential
# t: current temperature
# k: number of iterations so far
# step_size: number of iterations till temperature gets decreased
#  default = 100
# delta_t: value of temperature decreasing
#   if not specified exponential decreasing
# alpha_t: amount of current temperature to decrease
#   default = 0.1
#   not used if delta_t != 0
def cooling_shedule_linear_exp(t, k, step_size = 100, delta_t = 0, alpha_t = 0.1):
    if t <= 0: return 0
    if k % step_size: return t
    if delta_t != 0:
        return t - delta_t
    else:
        return t * (1 - alpha_t)

def cooling_shedule_sqrt(k, step_size = 100, t_start = 10):
    if k//step_size > t_start: return 0
    else: return sqrt(t_start - k//step_size)
    
def cooling_shedule_polynomial(k, n = 2, alpha = 0.1, step_size = 100, t_start = 10):
    return max([t_start - alpha * (k // step_size) ** n, 0])

def cooling_shedule_x3(k, step_size = 100):
    return -((k//step_size - 50) ** 3)/25000 + 5

###############################
#   IS_SOLUTION FUNCTIONS     #
###############################

# function testing for change in value in the last iterations
# c: history of values
# n: number of values to compare
#   default = 10
# epsilon: numeric boundary for change
#   if epsilon == 0: 1e-4 * c[-1] is used
def no_change(c, n = 10, epsilon = 0):
    if n > len(c): return False
    c_last = c[-1]
    if epsilon == 0:
        epsilon = abs(c_last * 1e-4)
    for i in range(n):
        if abs(c_last - c[-i-1]) > epsilon:
            return False
    return True


#########################
#   HELPER FUNCTIONS    #
#########################

# generator returning all possible combinations of arrays size = n with 1/-1
def get_cubes(n):
    if n == 0:
        yield []
    else:
        for x in get_cubes(n-1):
            yield [1] + x
            yield [-1] + x
            
# returns value of optimal solution
# param: J_0 = 1 for ferro magnet, -1 for anti
def get_optimal_solution(size, J_0 = -1):
    s = np.ones(size)
    if J_0 == 1: return get_H(s, 1)
    for x in range(size[0]):
        for y in range(size[1]):
            for z in range(size[2]):
                if (x+y+z)%2 == 1: s[x,y,z] = -1
    return get_H(s, -1)
