# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 21:05:37 2018

@author: Besitzer
"""

import numpy as np
import matplotlib.pyplot as plt
import utils
from PIL import Image
import imageio
import cv2

x_0 = np.random.rand(10,10,10)
x_0[x_0 < 0.5] = -1
x_0[x_0 >= 0.5] = 1
t_start = 10
n_stop = 1000
J_0 = -1
get_c = lambda s: get_H(s, J_0)
#get_t = lambda t, k: cooling_shedule_polynomial(k, n = 2, alpha = 0.001, step_size = 100, t_start = t_start)
#get_t = lambda t, k: cooling_shedule_sqrt(k, t_start = 10, step_size = 100)
get_t = lambda t, k: cooling_shedule_linear_exp(t, k, step_size = 200, delta_t = 0, alpha_t = 0.05)
is_solution = lambda c: no_change(c, n = n_stop)
#get_neighbour = lambda s: neighbour_axis_fill(s) #dont try
#get_neighbour = lambda s: neighbour_flip_point(s, 1)
#get_neighbour = lambda s: neighbour_floodfill_iterative(s)
get_neighbour = lambda s: optimize_neighbour(s, get_c) #needs debug
#
#optimal = get_optimal_solution(x_0.shape, J_0)
#c, t_history = simulated_annealing_alg(x_0, t_start, get_neighbour, get_c, utils.get_p_accept_standard, get_t, is_solution)
#
#fig, ax1 = plt.subplots()
#ax1.plot([optimal]*len(c), color = "red")
#ax1.plot(c, color = "b")
#ax1.set_xlabel("iterations")
#ax1.set_ylabel("H(s)", color = "b")
#ax1.tick_params("y", color = "b")
#
#ax2 = ax1.twinx()
#ax2.plot(t_history, color = "black")
#ax2.set_ylabel("temperature", color = "black")
#ax2.tick_params("y", color = "black")
#
#plt.annotate("J_0 = {} \nt_start = {} \nn_stop = {}".format(J_0, t_start, n_stop), (0.1, 0.15), xycoords = 'axes fraction')
#fig.legend(("optimal = {}".format(optimal), "H(s)", "temperature"), loc = 'best')
#fig.tight_layout()
#plt.savefig("test.png", dpi = 250)
#plt.close()
#
##s = np.random.rand(1, 10, 10)
##s[s < 0.5] = -1
##s[s >= 0.5] = 1
##print(s)
##get_c = lambda s: get_H(s, -1)
##n = neigbour_optimize_cube(s, get_c, size = 0)
##print(n)

def mean(x, k, c_y, J_0, scale = 20):
    a = x.shape[0]
    b = x.shape[1]
    result = np.zeros((a*scale + 10,b*scale))
    for i in range(a):
        for j in range(b):
            result[i*scale:(i+1)*scale,j*scale:(j+1)*scale] = min([2**8, int(sum([y for y in x[i,j,:] if y == 1])*(2**8)/x.shape[2])]) ** np.ones((scale, scale))
            cv2.putText(result, "H(s) = {}, J_0 = {}, iteration = {}".format(c_y, J_0, k),
                        (1, a * scale + 7), cv2.FONT_HERSHEY_SIMPLEX, 0.25, (255))
    return result


def simulated_annealing_alg(x_0, t_start, get_neighbour, get_c, get_p_accept, get_t, is_solution):
    # step 1: initialize algorithm
    x = x_0
    t = t_start
    k = 1           # count iterations
    c = [get_c(x)]  # history of state values
    t_history = [t_start]
    image_list = [mean(x, k, c[-1], J_0, scale = 20)]
    
    # algorithm loop - do while breaking criteria not fulfilled
    while True:    
        # step 2: get neighbour and evaluate stat
        y = get_neighbour(x)
        c_y = get_c(y)
  
        # step 3: test for replacing state
        if random() < get_p_accept(c[-1], c_y, t):
            x = y
            c.append(c_y)
        else:
            c.append(c[-1])
        t_history.append(t)
        
        # DEBUG
        if k % 100 == 0:
            print(c_y, t)
#            img = Image.new("L", x.shape[:2])
#            img.putdata(mean(x))
#            image_list.append(img)
            image_list.append(mean(x, k, c[-1], J_0, scale = 20))
        
        # step 4: get new temperature
        t = get_t(t, k)
        
        # step 5: test breaking criteria
        if is_solution(c):
            break
        else:
            k += 1
            
    imageio.mimsave("optimize_anti_10.gif", image_list, duration = 0.5)
    return c, t_history

c, t_history = simulated_annealing_alg(x_0, t_start, get_neighbour, get_c, utils.get_p_accept_standard, get_t, is_solution)
print(c[-1], len(c))








