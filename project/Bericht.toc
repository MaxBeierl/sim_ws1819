\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}% 
\contentsline {subsection}{\numberline {1.1}Simulationen}{1}{subsection.1.1}% 
\contentsline {subsection}{\numberline {1.2}Simulated Annealing}{2}{subsection.1.2}% 
\contentsline {subsection}{\numberline {1.3}Algorithmus}{2}{subsection.1.3}% 
\contentsline {subsection}{\numberline {1.4}Das Ising-Modell}{3}{subsection.1.4}% 
\contentsline {section}{\numberline {2}Modellierung}{4}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Nachbarschaft}{4}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Temperatur und Akzeptanzwahrscheinlichkeit}{5}{subsection.2.2}% 
\contentsline {section}{\numberline {3}Validierung}{7}{section.3}% 
