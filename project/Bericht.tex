\documentclass[12pt]{article}
% scrartcl ist eine abgeleitete Artikel-Klasse im Koma-Skript
% zur Kontrolle des Umbruchs Klassenoption draft verwenden

% die folgenden Packete erlauben den Gebrauch von Umlauten und ß
% in der Latex Datei
\usepackage[utf8]{inputenc}
% \usepackage[latin1]{inputenc} %  Alternativ unter Windows
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage{enumitem}
\usepackage[numbers]{natbib}
\setlist[enumerate]{label=\roman*)}


%Geschwungene Buchstaben
\usepackage{mathrsfs}
%Einrücken verhindern
\setlength{\parindent}{0em}

\usepackage[pdftex]{graphicx}
\usepackage{latexsym}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{color}

%InkScape einbinden
\usepackage{hyperref}
\usepackage[ngerman]{cleveref}
\usepackage{color}
\usepackage{transparent}
\graphicspath{{img/}}

%Zusammenfassung-Umgebung
\usepackage{abstract}

% Abstand obere Blattkante zur Kopfzeile ist 2.54cm - 15mm
\setlength{\topmargin}{-11mm}

%Kopfzeile
\usepackage{fancyhdr}


% Umgebungen für Definitionen, Sätze, usw.
% Es werden Sätze, Definitionen etc innerhalb einer Section mit
% 1.1, 1.2 etc durchnummeriert, ebenso die Gleichungen mit (1.1), (1.2) ..
\newtheoremstyle{remboldstyle}
{}{}{\itshape}{}{\bfseries}{.}{.5em}{{\thmname{#1 }}{\thmnumber{#2}}{\thmnote{ (#3)}}}
\theoremstyle{remboldstyle}
\newtheorem{Satz}{Satz}[section]
\newtheorem{Definition}[Satz]{Definition} 
\newtheorem{Lemma}[Satz]{Lemma}		  
\newtheorem{Proposition}[Satz]{Proposition} 
           
%Das Wort Beispiel ist fett gedruckt, der Text ist nicht kursiv
\theoremstyle{definition} 
\newtheorem{Beispiel}[Satz]{Beispiel}
       
\numberwithin{equation}{section} 

% einige Abkuerzungen
\newcommand{\C}{\mathbb{C}} % komplexe
\newcommand{\R}{\mathbb{R}} % reelle
\newcommand{\Q}{\mathbb{Q}} % rationale
\newcommand{\Z}{\mathbb{Z}} % ganze
\newcommand{\N}{\mathbb{N}} % natuerliche
\newcommand{\D}{\mathbb{D}} %Einheitskreisscheibe


\begin{document}
	
  % Keine Seitenzahlen im Vorspann
  %\pagestyle{empty}

  % Titelblatt der Arbeit
  \begin{titlepage}
%\includegraphics[width=\textwidth]{OTHLogo2.png}
    \vspace*{2cm} 

 \begin{center} \large 

    \textbf{{\huge Das Ising Modell}}
    \vspace*{2.5cm}

	\textbf{Hausarbeit}\\
	von Liridona Ademi, Maximilian Beierl und Hendwin Perdana\\
	im Studienfach Simulationen
	\vspace*{1.5cm}
	
    \vspace*{4.5cm}


    Ort, Datum: Regensburg, 17. Januar 2019
  \end{center}
\end{titlepage}

\newpage
\thispagestyle{empty}
\quad

\thispagestyle{empty}
\quad
\newpage


%Stil der Kopfzeile
\pagestyle{fancy}
\lhead{\slshape \rightmark}
\chead{}
\rhead{}

% Inhaltsverzeichnis
\tableofcontents
  
%subsection in Kopfzeile
\renewcommand{\subsectionmark}[1] {\markright{{\thesubsection\ #1}}} 
\newpage


  % Ab sofort Seitenzahlen in der Kopfzeile anzeigen

  \pagenumbering{arabic}
\setcounter{page}{1}
\section{Einleitung}
In vielen Bereichen der Mathematik erweist sich der Weg zur Lösung eines komplexen Problems oftmals als zeitaufwendig und anspruchsvoll. Möchte man ein mathematisches Modell lösen, welches als Optimierungsproblem vorliegt, so ist - wie der Name schon sagt - der optimalste Weg bzw. Algorithmus gesucht um ein zufriedenstellendes Ergebnis zu erhalten. Je nach Problemstellung müssen verschiedene Optimierungsalgorithmen zur Hilfe herangezogen werden. Entscheidend dabei sind vor allem die bereits vorliegenden Informationen. Systeme mit ausreichend vorliegenden Informationen führen häufig schneller zu einem optimalem Ergebnis als solche mit nur begrenzten Informationen. Zur Optimierung solcher Probleme werden beispielsweise sogenannten heuristische Approximationverfahren angewandt. Im Verlauf dieser Arbeit soll einer dieser Approximationsalgorithmen anhand eines Modells vorgestellt werden.

\subsection{Simulationen}
Mathematisches Modellieren ist in vielen Branchen eine weit verbreitete Notwendigkeit um Probleme aus der Realität, die nicht ohne weiteres gelöst werden können, zu lösen. Je nach zu modellierender Situation wird diese mit mathematischen Begriffen, Strukturen und Relationen so übersetzt, dass ein Fundament zur systematischen Lösung entsteht. Die im jeweiligen Modell angewandten mathematischen Methoden und Lösungen können rückführend auf das reale Problem angewandt werden. Änderungen der Realität bewirken ebensolche in dem mathematischen Modell, wodurch ein Kreislauf entsteht, in dem Problem und Modell immer aneinander angepasst werden müssen.
\\\\
Die Realisierung solcher Modelle kann sich je nach Komplexität des Problem als durchaus anspruchsvoll herausstellen. Vor allem in der Automobilbranche werden digitale Hilfsmittel herangezogen um die mathematischen Modelle zu simulieren. Eine solche \textbf{Simulation} ist demnach ein experimentelles System, dessen Ergebnisse auf die Wirklichkeit übertragbar sind. Zu den verschiedenen Arten der Simulationen gehören unter anderem die dynamischen Simulationen, welche vor allem zeitabhängige Modelle simulieren, statische Simulationen, welche zeitunabhängig simuliert werden und die stochastischen Simulationen, die bestimmte Wahrscheinlichkeitsannahmen, wie zum Beispiel Temperaturschwankungen, berücksichtigen.\cite{Greefrath}

\subsection{Simulated Annealing}
Der Begriff Annealing, welcher gleichbedeutend mit \glqq\textit{ausglühend}\grqq\, ist, stammt Ursprünglich aus der Glasherstellung bzw. aus Bereichen, in denen mit zu erhitzendem Stahl gearbeitet wird. Dort bezeichnet Annealing das gleichmäßige und kontrollierte Abkühlen des hergestellten Produkts. Durch diese angepasste Abkühlung haben die Moleküle, die sich durch das Erhitzen frei bewegen, Zeit eine möglichst stabile und energieoptimale Struktur zu erhalten. Sinn und Zweck dieses Prozesses ist es, dass das Endprodukt in dessen Gesamtheit stabiler wird. Bezogen auf die Mathematik wird mittels \textbf{Simulated Annealing} genau dieses Verfahren nachgebildet, welches sich für die Bearbeitung äußerst schwieriger Optimierungsproblemen als sehr nützlich herausstellt. Darunter fallen unter anderem geographische, chemische als auch physikalische Problemstellungen.
\\\\
Das \glqq \textit{Simulierte Glühen}\grqq\, ist ein leistungsfähiger Algorithmus, der aufgrund seiner Einfachheit und Effektivität häufig für die heuristische Optimierung verwendet wird. In diesem Ansatz werden die zu optimierenden Variablen als die Freiheitsgrade eines physikalischen Systems und die Kostenfunktion des Optimierungsproblems als Energie betrachtet. Man führt anschließend eine Monte-Carlo-Simulation dieses Systems durch, beginnend bei hohen Temperaturen und langsames Absenken der Temperatur während der Simulation, so dass letztendlich die Konfiguration des Systems in einem lokalen Minimum endet.\cite{Rossmanith}

\subsection{Algorithmus}\label{Algorithmus}
Iterative Verfahren haben häufig einen ähnlichen Aufbau. Ausgehend von einer Startsituation wird das System Schritt für Schritt verändert um am Ende bei einer besseren, bzw. optimalen Konfiguration zu landen. Dieser Prozess kann fortgeführt werden bis sich das System nicht mehr verändert oder die angestrebte Lösung erreicht wurde. Ähnlich ist auch der Algorithmus des Simulated Annealings aufgebaut. Der Simulated Annealing Algorithmus ist ein übersichtlicher und vergleichsweise einfacher Algorithmus. Zunächst wählt man für das vorliegende Problem eine erlaubte Startlösung. Nun wird aus der Nachbarschaft eine Lösung gewählt, deren Wert mit dem vorhergehenden verglichen wird. Je nach Akzeptanzwahrscheinlichkeit, welche von der Temperatur und der Wertedifferenz abhängt, wird dieser Wert dem ursprünglich gewählten Startwert überschrieben.

\\

Dieses Vorgehen ist in jeder gängigen Programmiersprache leicht zu implementieren. Gesucht ist ein $x \in \Omega$ welches die Funktion $C: \Omega \rightarrow \mathbb{R}$ minimiert (siehe \ref{Temperatur}). Dazu sind folgende Schritte der Reihenfolge nach durchzuführen:

\begin{itemize}
	\item[1)] Wähle $x:=x_0$ als Startwert mit Funktionswert $c_0 = C(x_0)$ und $t:=t_{start}$ als Starttemperatur
	\item[2)] Wähle ein $y$ aus der Nachbarschaft mit Funktionswert $c_{k+1} = C(y)$
	\item[3)] Setzte $x:=y$ mit Akzeptanzwahrscheinlichkeit $P(c_k,c_{k+1},t)$
	\item[4)] Berechne die neue Temperatur mittels Cooling-Schedule
	\item[5)] Je nach Abbruchkriterium wird das System beendet oder die Iteration mit $k=k+1$ im Schritt 2) fortgesetzt
\end{itemize}


\subsection{Das Ising-Modell}
Das Ising-Modell ist ein von Ernst Ising angefertigtes physikalisches Modell, welches die magnetischen Eigenschaften von Festkörpern beschreibt. Hervorzuheben ist, dass damit ebenso der Ferromagnetismus in Festkörpern beschrieben werden kann. Dieses Modell ist in der statistischen Physik ein sehr verbreitetes Modell, welches in mehreren Dimensionen Phasenübergänge zeigt. Die magnetischen Momente von Atomen werden durch eine klassische Spinvariable beschrieben. Da dieses Moment zwei mögliche Ausrichtungen hat, nämlich nach oben und nach unten, kann ein Spin auch zwei diskret Zustände $s_i=\pm 1$ annehmen. Die Energiefunktion ist gegeben durch

\begin{align}\label{H-Funk}
	H(\overrightarrow{s})=-\frac{1}{2}\sum J_{ij}s_is_j-H_n\sum_{i=1}^{n}s_i.
\end{align}

Für dieses Projekt wird auf den zweiten Term verzichtet und $H_n=0$ gesetzt. $J_{ij}$ ist hierbei die Kopplungskonstante zweier Spins und gibt die Wechselwirkung an. Es soll die Wechselwirkung für benachbarte Spins ungleich null gesetzt werden. Es ist somit

\begin{align*}
	J_{ij}=\begin{cases}
	J_0 & \text{, falls i,j benachbart}\\
	0 & \text{, sonst}
	\end{cases}
\end{align*}

Ist die Wechselwirkung positiv, also $J_{ij}=+1$, so wird das System eines Ferromagneten betrachtet; für $J_{ij}=-1$ ein Antiferromagnet.
Im folgenden Modell hat besitzt jeder innere Punkt sechs Nachbarn, d.h. die Punkte die über/unter, links/recht und vor/hinter ihm liegen.

\newpage

\section{Modellierung}
Nachfolgend soll nun ein Modellierungsansatz für ein System vorgestellt werden, welches die Energiefunktion für das Ising-Modell in drei Dimensionen minimiert. Hierbei wird das bereits vorgestellte Simulated Annealing zur Hilfe herangezogen und mittels Python implementiert.
\\\\
Wie bereits in \ref{Algorithmus} beschrieben, dient dieser Algorithmus als Grundstruktur. Je nach belieben wird zunächst die Größe des Magnets mittels eines 3-dimensionalen numpy arrays festgelegt und eine beliebige aber ausreichend hohe Starttemperatur gewählt.  Diese Startsituation wird nun durch die Energiefunktion des Ising-Modells ausgewertet. Auch hier kann nach belieben zwischen Ferromagnet und Antiferromagnet gewechselt werden. Anschließend wird die Wahl des Nachbars betrachtet.
Der Aufbau und die Struktur des Algorithmus wird durch drei elementare Bausteine bestimmt. Dazu gehören:
\begin{itemize}
	\item[1)] Die Wahl des Nachbarschaftsbegriffs, d.h. wie wird das $y$ in Schritt 2) des Simulated Annealing Algorithmus in \ref{Algorithmus} bestimmt
	\item[2)] Die Berechnung der Akzeptanzwahrscheinlichkeit
	\item[3)] Die Auswertung des Cooling-Schedule
\end{itemize}


\subsection{Nachbarschaft}
Gerade bei der Wahl geeigneter Nachbarn kann Hintergrundwissen über das System einfließen, was das Simulated Annealing effizienter machen kann.
Dazu sollen zunächst die verschiedenen Möglichkeiten für die Auswahl eines Nachbars vorgestellt werden. Eine Art der Auswahl ist es, einen zufällig gewählten Punkt im dreidimensionalem Raum zu nehmen und nur dessen Wert umzudrehen, also mit $-1$ zu multiplizieren. %neighbour_flip_point

Möchte man einen Schritt weiter gehen, so werden von diesem zufällig gewählten Punkt zusätzlich noch alle Nachbarn mit gleichem Spin ebenfalls umgedreht. Dabei wird jeder Nachbar überprüft und sollte dieser den gleichen Spin wie der Ausgangspunkt haben, so wird er gedreht und von da rekursiv fortgefahren, d.h. auch die Nachbarn dieses Punktes werden überprüft. Dieses Vorgehen eignet sich jedoch nur für den Ferromagneten, da somit Gebiete gleichen Spins gedreht werden und der ganze Magnet nach mehreren Iterationen einen einheitlichen Spin besitzt.
 \\
 Eine dritte Möglichkeit der Nachbarschaftswahl kann durch die Optimierung innerhalb eines Würfels erfolgen. Dazu wähle man sich einen zufällig gewählten Punkt im Raum und betrachte alle Würfel mit Seitenlänge $l$ (z. B. 1) in alle Richtungen. Der Magnet, welcher die beste Auswertung in \ref{H-Funk} liefert wird als neuer Nachbar gewählt. Jedoch wird schon hier klar, dass der Rechenaufwand je nach Wahl der Würfelseitenlänge zu enorm wird. (schon ein Würfel mit Seitenlänge 3 besitzt $2^{27} \approx 1,6 \cdot 10^8$ mögliche Zusammensetzungen). Daher soll bei der Validierung des Systems auf diese Nachbarschaftswahl vorerst verzichtet werden.
Ein weiterer Ansatz wäre, dass nur ein einzelner Punkt optimiert wird. Dabei besteht jedoch die Gefahr, dass man in ein lokales Minimum läuft, was sich beim Antiferromagneten ($J_0 = -1$) auch passiert.
 %neigbour optimize cube
 \\
 Ist die Wahl des Nachbarns beendet, so wird auch dieser Systemstatus durch die Energiefunktion des Ising-Modells ausgewertet. Nach der Reihenfolge des Simulated Annealing Algorithmus wird nun die Akzeptanzwahrscheinlichkeit \ref{Akzeptanzwahrscheinlichkeit} berechnet und dem entsprechend der Status des Ausgangssystems mit dem des neuen ersetzt. Anschließen muss die Korrektur der Temperatur nach der Cooling-Schedule \ref{Cooling} erfolgen. Abschließend muss nun geprüft werden ob ein weiterer Iterationsschritt der Simulation durchlaufen wird oder nicht. Dazu muss ein Abbruchkriterium festgelegt werden. Dazu wurden alle bisherigen Energiefunktionswerte der Systemstati in einem Array gespeichert und die Differenz zweier aufeinanderfolgender Werte betrachtet. Ist die Differenz größer wird mit dem nächsten Iterationsschritt fortgefahren, ansonsten wird abgebrochen. Wann der Differenzwert als hoch bzw. niedrig gilt muss selber beschlossen werden. Eine geeignete Richtlinie kann $10^{-4}$ sein. Ebenso kann auch eine gewisse Anzahl von Iterationsschritten als Abbruchkriterium geeignet sein.

\subsection{Temperatur und Akzeptanzwahrscheinlichkeit} \label{Temperatur}

Die Berechnung der Akzeptanzwahrscheinlichkeit muss an die gegebene Problemstellung angepasst werden, jedoch kommen nur bestimmte Funktionen in Frage.
Sei $\Omega$ der gesamte Zustandsraum des Systems (hier: $\{-1, 1\}^{l \cdot b \cdot h}$ wobei $l,b,h$ den Dimensionen des Magneten entsprechen) und 
\begin{align*}
	C:\Omega \rightarrow A \subseteq \mathbb{R}
\end{align*}
die zu optimierende Funktion. Weiterhin sei 
\begin{align*}
	(T_k)_{k \in \mathbb{N}} := T
\end{align*}
die Menge der auftretenden Temperaturen, bzw. die fallende Folge der Temperaturen der jeweiligen Iterationsschritte. Folglich muss für die Akzeptanzfunktion gelten:
\begin{align*}
	P: A \times A \times T \rightarrow [0,1]
\end{align*}


Für das Simulated Annealing wird die Standard-Akzeptanzwahrscheinlichkeit

\begin{align}\label{Akzeptanzwahrscheinlichkeit}
	P(c_k,c_{k+1},t)=\begin{cases}
	1 & \text{, für } c_{k+1}\le c_k\\
	e^{\frac{c_k-c_{k+1}}{t}} & \text{, sonst} 
	\end{cases}
\end{align}
\\
angewandt, wobei $t$ der aktuellen Temperatur entsprecht. 
\\
Der Standard-Coolingschedule ist wie folgt definiert:

\begin{align}\label{Cooling}
	T(k,t)=\begin{cases}
	0 & \text{, für } t\le 0\\
	t-\triangle t & \text{,} k\equiv 0 \text{ (mod K)} \\
	t & \text{, sonst}
	\end{cases}
\end{align}
\\
wobei $K, \triangle t$ fest zu wählen sind.
\\\\
Auch hier ist es - genau wie beim Nachbarschaftsbegriff - möglich, einen geeigneten anderen Cooling-Schedule zu definieren. Anstatt eines linearen Abfalls kann auch ein exponentieller gewählt werden. Eine mögliche Umsetzung bietet

\begin{align}
		T(k,t)=t \cdot (1-\alpha)^{\lfloor \frac{k}{K} \rfloor}
\end{align}
mit reellem $\alpha \in (0,1)$ und Schrittweite $K$.
\\\\
Da zu beobachten ist, dass bei manchen Temperaturen die größten Verbesserungen erzielt werden, liegt es nahe, einen Coolingschedule zu definieren, der sich möglichst lange bei diesen Temperaturen aufhält. Dazu wird ein Polynom dritten Grades verwendet und der Sattelpunkt auf die optimale Temperatur gelegt. Da sich der Bereich [2,8] als besonders effizient herausgestellt hat (siehe \ref{optimale Temperatur}), wird eine weitere Möglichkeit wie folgt definiert:
\begin{align}
	T(k) = -\frac{(\lfloor \frac{k}{K} \rfloor - 50)^3}{25000} + 5 \text{ für } k \leq 100K \text{, } 0 \text{ sonst}
\end{align}
Dabei gilt $T(\mathbb{N}_0) \subseteq [0, 10]$
\\\\
Offensichtlich führen die Veränderungen von Nachbarschaftsbegriffen, Cooling-Schedule oder gar die Größe des Magnets zu Änderungen der Iterationsergebnisse. Im Nachfolgendem Kapitel sollen die Ergebnisse solcher Veränderungen noch aufgezeigt werden und unter den hier vorgestellten Varianten die ausgewählt werden, bei der die besten Ergebnisse erzielt werden können.

\newpage
\section{Validierung}

\label{optimale Temperatur}

TODO: floodfill

  \newpage
  \thispagestyle{empty}
  \quad
  \newpage

  \listoffigures
  \newpage

  
    % Literaturverzeichnis (beginnt auf einer ungeraden Seite)
   \bibliographystyle{plaindin}
	\bibliography{SimLiteratur}


\end{document}

