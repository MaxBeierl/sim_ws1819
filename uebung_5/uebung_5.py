from random import random
import matplotlib.pyplot as plt

#################
#	Aufgabe 2	#
#################


def n_s_alg(street, v_max, p):
	for i in range(len(street)):
		if street[i] > -1:
			street[i] = min(v_max, street[i]+1)
			j = 1
			while(j <= street[i]):
				if i + j == len(street): break
				if street[i+j]: street[i] = j - 1
			if random() < p: street[i] -= 1
			if i + street[i] >= len(street):
				street[0] = int(random() * (v_max+1))
				street[i] = -1
			street[i + street[i]] = street[i]
			street[i] = -1
	return street


street = [-1] * 10
density = 0.16
p = 0
v_max = 5
N = 10
for i in range(len(street)):
	if random() < density: street[i] = int(random() * (v_max+1))

plt.plot(street, [0]*len(street))
for i in range(N):
        print(street)
        street = n_s_alg(street, v_max, p)
        plt.plot(street, [i+1]*len(street))
plt.show()
